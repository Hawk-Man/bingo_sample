_$ = function(x) {
  return document.getElementById(x);
}

// ビンゴ用数字配列
var numList = [ "01","02","03","04","05",
                "06","07","08","09",10,
                11,12,13,14,15,
                16,17,18,19,20,
                21,22,23,24,25,
                26,27,28,29,30,
                31,32,33,34,35,
                36,37,38,39,40,
                41,42,43,44,45,
                46,47,48,49,50,
                51,52,53,54,55,
                56,57,58,59,60,
                61,62,63,64,65,
                66,67,68,69,70,
                71,72,73,74,75
              ];

var isStop = true;

function startBingo() {
  // ボタンの表示切り替え
  _$("start").style.display = "none";
  _$("stop").style.display = "inline";
  isStop = false;
  roulette();
}

function stopBingo() {
  // ボタンの表示切り替え
  _$("start").style.display = "inline";
  _$("stop").style.display = "none";
  isStop = true;
}

function roulette() {
  var id = "";
  var rnd = Math.floor(Math.random() * numList.length);

  // ストップボタンが押された
  if (isStop) {
    // 遅延呼び出しを解除
    clearTimeout(id);

    _$("view").innerHTML = numList[rnd];
    if (!_$("out").innerHTML) {
      _$("out").innerHTML = _$("out").innerHTML + numList[rnd];
    }
    else {
      _$("out").innerHTML = _$("out").innerHTML + "　" + numList[rnd];
    }

    // 決定した数字をリストから削除する
    numList.splice(rnd, 1);
    // リストが空になったら終了
    if (numList.length == 0) {
      alert("最後です。");
      _$("start").disabled = true;
    }
    return false;
  }

  // 乱数を画面に表示
  _$("view").innerHTML = numList[rnd];
  // 50ms後に再帰的に実行するよう登録する
  id = setTimeout("roulette()", 50);
}

function hyoji1(num) {
  if (num == 0 ) {
    document.getElementById("disp").style.display="block";
  } else {
    document.getElementById("disp").style.display="none";
  }
}

// 景品

// showPrizeName();
//
// function showPrizeName() {
//   // 表示・非表示
//   $(".checkBox").on('change', function() {
//     var display = $(this).siblings(".display");
//     var state = $(this).prop('checked');
//     if (state) {
//       display.removeClass('hide');
//     } else {
//       display.addClass('hide');
//     }
//   });
// }
