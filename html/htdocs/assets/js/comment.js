$(document).ready(function(){

  var masterData;//取得したjsonデータ保存用
  var tempData ;//処理用のjson
  var commentHtml;//jsonから生成したHTML
  var commentDom;//挿入するdom用
  var interval = 4000;//コメント追加時間
  var commentDisplayTime = 20000;//コメント表示時間
  var commentType = 1;//コメントの種類管理用（全７種類）
  var maxComment = 20;//コメント表示数（新着順）



  // プログラム変更（キーボート上下キー）
  $(window).keydown(function(e){
    var $programItem = $('.js-programItem').find('li');
    var $thisCueernt = $('.js-programItem').find('.current');
    if(e.keyCode == 38 && !($thisCueernt.text() == $programItem.first().text())) {
      // 上キー押された時かつ最初の値ではない時の処理
      $thisCueernt.removeClass('current');
      $thisCueernt.prev().addClass('current');
    }else if(e.keyCode == 40 && !($thisCueernt.text() == $programItem.last().text())) {
      // 下キー押された時かつ最後の値ではない時の処理
      $thisCueernt.removeClass('current');
      $thisCueernt.next().addClass('current');
    }
  });
  // プログラム変更（要素クリック）
  $('.js-programItem').find('li').on('click', function() {
    $(this).siblings().removeClass('current');
    $(this).addClass('current');
  });

  getSheetEveryone();
  // データ取得までの時間稼ぎ
  $(function(){
    setTimeout(function(){
      tempData = JSON.parse(JSON.stringify(masterData));
      commentDisplay();
    },1000);
  });

  // スプレッドシートからみんなのコメント取得
  function getSheetEveryone(){
      $.ajax({
        type: 'GET',
        url: 'https://spreadsheets.google.com/feeds/cells/1lsF8TAH8GmRL30Lf6fob3SSaflcZAlnkmpO3g9XbMok/ok0cimp/public/values?alt=json',
        dataType: 'jsonp',
        cache: false,
        success: function(data){ // 通信が成功した時
          var sheetsEntry = data.feed.entry; // 実データ部分を取得
          var getData;
          getData = categorizeData(sheetsEntry); // データを整形して配列で返す
          // commentTypeを追加
          for (var i = 0; i < getData.length; i++) {
            getData[i].commentType = '2';
          }
          masterData = getData;
        },
        error: function(){ // 通信が失敗した時
          console.log('error');
        }
      });
  }

  // データを整形して配列で返す
  function categorizeData(sheetsEntry){
    var keyWord = [];
    var categorized = [];
    for(var i = 0; i < sheetsEntry.length; i++) {
      var dataCol = sheetsEntry[i].gs$cell.col;
      var dataRow = sheetsEntry[i].gs$cell.row;
      var dataTxt = sheetsEntry[i].gs$cell.$t;
      if(dataRow == 1) {
        keyWord.push(dataTxt);
      }else if(dataCol == 1){
        var item = {};
        var itemKey = keyWord[0];
        item[itemKey] = dataTxt;
        categorized.push(item);
      }else {
        var targetObject = categorized[dataRow-2];
        var itemKey = keyWord[dataCol-1];
        targetObject[itemKey] = dataTxt;
      }
    }
    return categorized;
  }

  // dom表示
  function commentDisplay() {
    // パラメータが無くなっていれば追加
    if(Object.keys(tempData).length == 0 ) {
      getSheetEveryone();
      tempData = JSON.parse(JSON.stringify(masterData));
    }else if(Object.keys(tempData).length == Object.keys(masterData).length -maxComment) {
      getSheetEveryone();
      tempData = JSON.parse(JSON.stringify(masterData));
    }

    // domの生成へ
    param = tempData[Object.keys(tempData).length-1];
    //jsonを基にHTML生成
    if( 7 < commentType) commentType = 1;
    commentHtml = '<li class="commontType' + commentType + '">' + param.comment + '</li>';
    commentType ++;
    // アニメーションの追加
    commentDom = $(commentHtml).fadeIn(400).delay(commentDisplayTime).fadeOut(400, function() {
      $(this).remove();
    });;
    // DOMに追加
    $('.js-commentItem').append(commentDom);
    // 処理済みのパラメータ削除
    tempData.pop();
    // 次の回の実行予約
    setTimeout(function(){
        commentDisplay();
    }, interval);
  }




});