$(document).ready(function(){

  var masterData;//取得したjsonデータ保存用
  var tempData ;//処理用のjson
  var commentHtml;//jsonから生成したHTML
  var commentDom;//挿入するdom用
  var interval = 15000;//コメント追加時間
  var commentDisplayTime = 20000;//コメント表示時間

  getSheet();
  // データ取得までの時間稼ぎ
  $(function(){
    setTimeout(function(){
      tempData = JSON.parse(JSON.stringify(masterData));
      commentDisplay();
    },1000);
  });

  // スプレッドシートデータの取得
  $('.js-updateBtn').on('click', function() {
    getSheet();
  });


  // スプレッドシートからデータ取得
  function getSheet(){
      $.ajax({
        type: 'GET',
        url: 'https://spreadsheets.google.com/feeds/cells/1lsF8TAH8GmRL30Lf6fob3SSaflcZAlnkmpO3g9XbMok/od6/public/values?alt=json',
        dataType: 'jsonp',
        cache: false,
        success: function(data){ // 通信が成功した時
          var sheetsEntry = data.feed.entry; // 実データ部分を取得
          masterData = categorizeData(sheetsEntry); // データを整形して配列で返す
        },
        error: function(){ // 通信が失敗した時
          console.log('error');
        }
      });
  }

  // データを整形して配列で返す
  function categorizeData(sheetsEntry){
    var keyWord = [];
    var categorized = [];
    for(var i = 0; i < sheetsEntry.length; i++) {
      var dataCol = sheetsEntry[i].gs$cell.col;
      var dataRow = sheetsEntry[i].gs$cell.row;
      var dataTxt = sheetsEntry[i].gs$cell.$t;
      if(dataRow == 1) {
        keyWord.push(dataTxt);
      }else if(dataCol == 1){
        var item = {};
        var itemKey = keyWord[0];
        item[itemKey] = dataTxt;
        categorized.push(item);
      }else {
        var targetObject = categorized[dataRow-2];
        var itemKey = keyWord[dataCol-1];
        targetObject[itemKey] = dataTxt;
      }
    }
    return categorized;
  }

  // dom表示
  function commentDisplay() {
    // パラメータが無くなっていれば追加
    if(Object.keys(tempData).length == 0 ) {
      tempData = JSON.parse(JSON.stringify(masterData));
    }

    // domの生成へ
    param = tempData[Object.keys(tempData).length-1];
    //jsonを基にHTML生成
    switch(param.commentType) {
      // 速報コメント用
      case '1':
        commentHtml ='<li>'
                  + param.contributorName + '：'
                   +'「' + param.winnerName + '」さんが'
                   +'商品の【' + param.ProductName + '】をゲットいたしましたー！'
                   +'引いたのは「' + param.bingoName + '」さん！おめでとうございます！'
                   +'</li>';
        break;
      // 通常コメント用
      case '2':
        commentHtml ='<li>' + param.comment + '</li>';
        break;
      default:
        commentHtml ='<li>'
                    +'<div>楽しんでね！（データミス、、、、）</div>'
                    +'</li>';
        break;
    }
    // アニメーションの追加
    commentDom = $(commentHtml).delay(commentDisplayTime).remove();
    // DOMに追加
    $('.js-commentItem').append(commentDom);
    // 処理済みのパラメータ削除
    tempData.pop();
    // 次の回の実行予約
    setTimeout(function(){
        commentDisplay();
    }, interval);
  }




});