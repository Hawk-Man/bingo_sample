# README #

ビンゴツール エンジニアwiki

### What is this repository for? ###

* Version 2017(2.0)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Premise ###
- gulpがインストール済
- yarnがインストール済

## Install the library
作業フォルダで以下コマンドを実行する。

```
sudo npm install -g gulp
```
```
yarn install
```
```
npm install browser-sync gulp-if gulp-plumber gulp-sass gulp-concat gulp-autoprefixer gulp-minify-css gulp-watch gulp-notify
```

### How do I get set up? ###

* クローンする
* gulpをインストールする
* yarnをインストールする
* gulpの各プラグインをインストールする

## インデント（HTML/JS/CSS共通）
半角スペース2個

## 文字コード
UTF-8

## 改行コード
CRLF

## CSS クラス命名規則
基本BEMベース

### デフォルトタスク

```
glup
```

* デフォルトタスクでscss・html・jsを監視する
* 変更があればscss・jsをビルドし直す
* また、browser-syncで目次ページが起動するようになっている

# auto-prefixer
* auto-prefixerを導入しているので、ベンダプレフィックスは気にしなくて良い
* auto-prefixerの設定はconfig.jsにあるので適宜変更する

# cssの圧縮
* config.jsのminifyを**true**にしてビルドする
